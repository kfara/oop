<?php
    require("animal.php");
    require("ape.php");
    require("frog.php");

    $sheep = new animal("shaun");
        echo "Name : " . $sheep->nama . "<br>";
        echo "Legs : " . $sheep->legs . "<br>";
        echo "Cold blooded : " . $sheep->cold_blooded . "<br><br>";
    
    $sungokong = new ape("kera sakti");
        echo "Name : " . $sungokong->nama . "<br>";
        echo "Legs : " . $sungokong->legs . "<br>";
        echo "Cold blooded : " . $sungokong->cold_blooded . "<br>";
        echo $sungokong->suara("Auooo") . "<br><br>";

    $kodok = new frog("buduk");
        echo "Name : " . $kodok->nama . "<br>";
        echo "Legs : " . $kodok->legs . "<br>";
        echo "Cold blooded : " . $kodok->cold_blooded . "<br>";
        echo $kodok->lompat("hop hop") . "<br>";
?>